package app.directors;

import app.builders.DifficultySettingsBuilder;

public class DifficultySettingsDirector {

    private DifficultySettingsBuilder builder;

    public DifficultySettingsDirector(DifficultySettingsBuilder builder) {
        this.builder = builder;
    }

    public DifficultySettingsBuilder getEasyModePreset() {
        builder.enemyHPMultiplier(1);
        builder.enemyDamageMultiplie(1);
        builder.permadeath(false);
        return builder;
    }

    public DifficultySettingsBuilder getHardcoreModePreset() {
        builder.enemyHPMultiplier(3);
        builder.enemyDamageMultiplie(3);
        builder.permadeath(false);
        return builder;
    }
}
