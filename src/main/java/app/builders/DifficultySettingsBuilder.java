package app.builders;

public interface DifficultySettingsBuilder {

    DifficultySettingsBuilder enemyHPMultiplier(int multiplier);
    DifficultySettingsBuilder enemyDamageMultiplie(int multiplier);
    DifficultySettingsBuilder permadeath(boolean isOn);
    DifficultySettings build();
}
