package app.builders;

public class DifficultySettingsBuilderImpl implements DifficultySettingsBuilder {
    private double enemyHPMultiplier;
    private double enemyDamageMultiplier;
    private boolean permadeath;

    @Override
    public DifficultySettingsBuilder enemyHPMultiplier(int multiplier) {
        this.enemyHPMultiplier = multiplier;
        return this;
    }

    @Override
    public DifficultySettingsBuilder enemyDamageMultiplie(int multiplier) {
        this.enemyDamageMultiplier = multiplier;
        return this;
    }

    @Override
    public DifficultySettingsBuilder permadeath(boolean isOn) {
        this.permadeath = isOn;
        return this;
    }

    @Override
    public DifficultySettings build() {
        return new DifficultySettings(enemyHPMultiplier, enemyDamageMultiplier, permadeath);
    }
}
