package app.builders;

import java.util.Objects;

public class DifficultySettings {
    private final double enemyHPMultiplier;
    private final double enemyDamageMultiplier;
    private final boolean permadeath;

    protected DifficultySettings(double enemyHPMultiplier, double enemyDamageMultiplier, boolean permadeath) {
        this.enemyHPMultiplier = enemyHPMultiplier;
        this.enemyDamageMultiplier = enemyDamageMultiplier;
        this.permadeath = permadeath;
    }

    public double getEnemyHPMultiplier() {
        return enemyHPMultiplier;
    }

    public double getEnemyDamageMultiplier() {
        return enemyDamageMultiplier;
    }

    public boolean isPermadeath() {
        return permadeath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DifficultySettings that = (DifficultySettings) o;
        return Double.compare(getEnemyHPMultiplier(), that.getEnemyHPMultiplier()) == 0 && Double.compare(getEnemyDamageMultiplier(), that.getEnemyDamageMultiplier()) == 0 && isPermadeath() == that.isPermadeath();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEnemyHPMultiplier(), getEnemyDamageMultiplier(), isPermadeath());
    }

    @Override
    public String toString() {
        return "DifficultySettings{" +
                "enemyHPMultiplier=" + enemyHPMultiplier +
                ", enemyDamageMultiplier=" + enemyDamageMultiplier +
                ", permadeath=" + permadeath +
                '}';
    }
}
