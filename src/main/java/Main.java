import app.builders.DifficultySettings;
import app.builders.DifficultySettingsBuilder;
import app.builders.DifficultySettingsBuilderImpl;
import app.directors.DifficultySettingsDirector;

public class Main {

    // An example of Client Code usage.
    public static void main(String[] args) {
        DifficultySettingsBuilder builder = new DifficultySettingsBuilderImpl();
        DifficultySettingsDirector director = new DifficultySettingsDirector(builder);

        DifficultySettingsBuilder difficultyPreset = director.getHardcoreModePreset();

        difficultyPreset.permadeath(true);

        DifficultySettings difficultySettings = difficultyPreset.build();

        System.out.println(difficultySettings);
    }
}
