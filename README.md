# Difficulty Settings Builder

## Description
An example of a Builder pattern usage. This implementation uses director\
to help with an object creation. 

In this example Director doesn't create an object, but rather prepares\
the state of the builder which allows client to make additional changes\
before final instantiation.